import Vue from 'vue'

export default {
  state: {
    items:  [
      {
        tipo: 'Calça',
        titulo: 'Calça Levi`s 550',
        valor_antes: 'R$ 350,00',
        valor_depois: 'R$ 129,90',
        off: '63%',
        cor: 'Azul claro',
        tamanhos: ['30-34', '36-38', '40-44', '46-50', '50+'],
        descr: 'Calça jeans preta descolada, perfeito para passeio.',
        localizacao: 'Shopping Estação - Loja: 06; Kira`s Clothing',
        telefone: '(41) 3847-9372',
        nome_img: 'Calca_Masculina.png',
        sexo: 'masculino'
      },
      {
        tipo: 'Calça',
        titulo: 'Calça Jeans Crawford',
        valor_antes: 'R$180,00',
        valor_depois: 'R$75,00',
        off: '58%',
        cor: 'Azul claro',
        tamanhos: ['30-34', '36-38', '40-44', '46-50', '50+'],
        descr: 'Calça jeans azul clara descolada, perfeita para passeio.',
        localizacao: 'Shopping Estação - Loja: 06; Kira’s Clothing',
        telefone: '(41) 3847-9372',
        nome_img: 'Calca_Feminina.png',
        sexo: 'feminino'
      },
      {
        tipo: 'Calça',
        titulo: 'Calça de Sarja',
        valor_antes: 'R$220,00',
        valor_depois: 'R$80,00',
        off: '64%',
        cor: 'Marrom escuro',
        tamanhos: ['30-34', '36-38', '40-44', '46-50', '50+'],
        descr: 'Calça de sarja descolada, perfeito para passeio.',
        localizacao: 'Shopping Estação - Loja: 06; Kira`s Clothing',
        telefone: '(41) 3847-9372',
        nome_img: 'Calca_Masculina_Sarja.png',
        sexo: 'masculino'
      },
      {
        tipo: 'Calça',
        titulo: 'Calça Camuflada Kiron',
        valor_antes: 'R$150,00',
        valor_depois: 'R$ 70,00',
        off: '54%',
        cor: 'Camuflagem',
        tamanhos: ['30-34', '36-38', '40-44', '46-50', '50+'],
        descr: 'Com essa calça, some ao ambiente com muito estilo.',
        localizacao: 'Shopping Estação - Loja: 06; Kira`s Clothing',
        telefone: '(41) 3847-9372',
        nome_img: 'Calca_Masculina_Exercito.png',
        sexo: 'masculino'
      },
      {
        tipo: 'Calça',
        titulo: 'Calça Black Sarja',
        valor_antes: 'R$189,90',
        valor_depois: 'R$65,00',
        off: '66% ',
        cor: 'Preto',
        tamanhos: ['30-34', '36-38', '40-44', '46-50', '50+'],
        descr: 'Calça com caimento perfeito para o trabalho e dia-a-dia.',
        localizacao: 'Shopping Estação - Loja: 06; Kira`s Clothing',
        telefone: '(41) 3847-9372',
        nome_img: 'Calca_Masculina_Preta.png',
        sexo: 'masculino'
      },
      {
        tipo: 'Calça',
        titulo: 'Calça Jeans Destroyed',
        valor_antes: 'R$160,00',
        valor_depois: 'R$60,00',
        off: '63% ',
        cor: 'Azul',
        tamanhos: ['30-34', '36-38', '40-44', '46-50', '50+'],
        descr: 'Calça com caimento perfeito para o trabalho e dia-a-dia.',
        localizacao: 'Shopping Estação - Loja: 06; Kira`s Clothing',
        telefone: '(41) 3847-9372',
        nome_img: 'Calca_Masculina_Jeans_Destroyed.png',
        sexo: 'masculino'
      },
      {
        tipo: 'Calça',
        titulo: 'Calça Jeans Clara',
        valor_antes: 'R$239,90',
        valor_depois: 'R$109,90',
        off: '54% ',
        cor: 'Azul',
        tamanhos: ['30-34', '36-38', '40-44', '46-50', '50+'],
        descr: 'Calça com caimento perfeito para o trabalho e dia-a-dia.',
        localizacao: 'Shopping Estação - Loja: 06; Kira`s Clothing',
        telefone: '(41) 3847-9372',
        nome_img: 'Calca_Masculina_Jeans_Clara.png',
        sexo: 'masculino'
      },
      {
        tipo: 'Calça',
        titulo: 'Calça Jeans Marrom',
        valor_antes: 'R$189,90',
        valor_depois: 'R$89,90',
        off: '53% ',
        cor: 'Azul',
        tamanhos: ['30-34', '36-38', '40-44', '46-50', '50+'],
        descr: 'Calça com caimento perfeito para o trabalho e dia-a-dia.',
        localizacao: 'Shopping Estação - Loja: 06; Kira`s Clothing',
        telefone: '(41) 3847-9372',
        nome_img: 'Calca_Masculina_Jeans_Marrom.png',
        sexo: 'masculino'
      },
      {
        tipo: 'Calça',
        titulo: 'Calça Skinny Marrom',
        valor_antes: 'R$259,90',
        valor_depois: 'R$119,90',
        off: '54% ',
        cor: 'Marrom',
        tamanhos: ['30-34', '36-38', '40-44', '46-50', '50+'],
        descr: 'Calça com caimento perfeito para o trabalho e dia-a-dia.',
        localizacao: 'Shopping Estação - Loja: 06; Kira`s Clothing',
        telefone: '(41) 3847-9372',
        nome_img: 'Calca_Masculina_Skinny_Marrom.png',
        sexo: 'masculino'
      },
      {
        tipo: 'Calça',
        titulo: 'Calça Skinny Vermelha',
        valor_antes: 'R$299,90',
        valor_depois: 'R$119,90',
        off: '60% ',
        cor: 'Vermelha',
        tamanhos: ['30-34', '36-38', '40-44', '46-50', '50+'],
        descr: 'Calça com caimento perfeito para o trabalho e dia-a-dia.',
        localizacao: 'Shopping Estação - Loja: 06; Kira`s Clothing',
        telefone: '(41) 3847-9372',
        nome_img: 'Calca_Masculina_Skinny_Vermelha.png',
        sexo: 'masculino'
      },
      {
        tipo: 'Calça',
        titulo: 'Calça Skinny Branca',
        valor_antes: 'R$179,90',
        valor_depois: 'R$69,90',
        off: '61% ',
        cor: 'Branca',
        tamanhos: ['30-34', '36-38', '40-44', '46-50', '50+'],
        descr: 'Calça com caimento perfeito para o trabalho e dia-a-dia.',
        localizacao: 'Shopping Estação - Loja: 06; Kira`s Clothing',
        telefone: '(41) 3847-9372',
        nome_img: 'Calca_Masculina_Skinny_Branca.png',
        sexo: 'masculino'
      },
      {
        tipo: 'Calça',
        titulo: 'Calça Red Velvet',
        valor_antes: 'R$180,00',
        valor_depois: 'R$70,00',
        off: '62% ',
        cor: 'Vermelho',
        tamanhos: ['30-34', '36-38', '40-44', '46-50', '50+'],
        descr: 'Calça com caimento perfeito para o trabalho e dia-a-dia.',
        localizacao: 'Shopping Estação - Loja: 06; Kira`s Clothing',
        telefone: '(41) 3847-9372',
        nome_img: 'Calca_Feminina_Vermelho.png',
        sexo: 'feminino'
      },
      {
        tipo: 'Calça',
        titulo: 'Calça Verde Jack`s',
        valor_antes: 'R$245,00',
        valor_depois: 'R$90,00',
        off: '64% ',
        cor: 'Verde',
        tamanhos: ['30-34', '36-38', '40-44', '46-50', '50+'],
        descr: 'Calça com caimento perfeito para o trabalho e dia-a-dia.',
        localizacao: 'Shopping Estação - Loja: 06; Kira`s Clothing',
        telefone: '(41) 3847-9372',
        nome_img: 'Calca_Feminina_Verde.png',
        sexo: 'feminino'
      },
      {
        tipo: 'Calça',
        titulo: 'Calça Black Hanger',
        valor_antes: 'R$140,00',
        valor_depois: 'R$60,00',
        off: '58% ',
        cor: 'Preto',
        tamanhos: ['30-34', '36-38', '40-44', '46-50', '50+'],
        descr: 'Calça com caimento perfeito para o trabalho e dia-a-dia.',
        localizacao: 'Shopping Estação - Loja: 06; Kira`s Clothing',
        telefone: '(41) 3847-9372',
        nome_img: 'Calca_Feminina_Preta.png',
        sexo: 'feminino'
      },
      {
        tipo: 'Calça',
        titulo: 'Calça Camuflada',
        valor_antes: 'R$119,90',
        valor_depois: 'R$59,90',
        off: '51% ',
        cor: 'Verde',
        tamanhos: ['30-34', '36-38', '40-44', '46-50', '50+'],
        descr: 'Calça com caimento perfeito para o trabalho e dia-a-dia.',
        localizacao: 'Shopping Estação - Loja: 06; Kira`s Clothing',
        telefone: '(41) 3847-9372',
        nome_img: 'Calca_Feminina_Camuflada_Verde.png',
        sexo: 'feminino'
      },
      {
        tipo: 'Calça',
        titulo: 'Calça Jeans',
        valor_antes: 'R$99,90',
        valor_depois: 'R$39,90',
        off: '51% ',
        cor: 'Amarela',
        tamanhos: ['30-34', '36-38', '40-44', '46-50', '50+'],
        descr: 'Calça ótima para passeio.',
        localizacao: 'Shopping Estação - Loja: 06; Kira`s Clothing',
        telefone: '(41) 3847-9372',
        nome_img: 'Calca_Feminina_Jeans_Amarela.png',
        sexo: 'feminino'
      },
      {
        tipo: 'Calça',
        titulo: 'Calça Legging',
        valor_antes: 'R$109,90',
        valor_depois: 'R$49,90',
        off: '55% ',
        cor: 'Preta',
        tamanhos: ['30-34', '36-38', '40-44', '46-50', '50+'],
        descr: 'Calça ótima para passeio.',
        localizacao: 'Shopping Estação - Loja: 06; Kira`s Clothing',
        telefone: '(41) 3847-9372',
        nome_img: 'Calca_Feminina_Legging_Preta.png',
        sexo: 'feminino'
      },
      {
        tipo: 'Calça',
        titulo: 'Calça Skinny',
        valor_antes: 'R$119,90',
        valor_depois: 'R$49,90',
        off: '58% ',
        cor: 'Branca',
        tamanhos: ['30-34', '36-38', '40-44', '46-50', '50+'],
        descr: 'Calça ótima para passeio.',
        localizacao: 'Shopping Estação - Loja: 06; Kira`s Clothing',
        telefone: '(41) 3847-9372',
        nome_img: 'Calca_Feminina_Skinny_Branca.png',
        sexo: 'feminino'
      }, 
      {
        tipo: 'Calça',
        titulo: 'Calça Skinny',
        valor_antes: 'R$219,90',
        valor_depois: 'R$99,90',
        off: '55% ',
        cor: 'Marrom',
        tamanhos: ['30-34', '36-38', '40-44', '46-50', '50+'],
        descr: 'Calça ótima para o dia-a-dia e conforto.',
        localizacao: 'Shopping Estação - Loja: 06; Kira`s Clothing',
        telefone: '(41) 3847-9372',
        nome_img: 'Calca_Feminina_Skinny_Marrom.png',
        sexo: 'feminino'
      }, 
      {
        tipo: 'Calça',
        titulo: 'Calça Social Preta e Branca',
        valor_antes: 'R$259,90',
        valor_depois: 'R$99,90',
        off: '62% ',
        cor: 'Preta e branca',
        tamanhos: ['30-34', '36-38', '40-44', '46-50', '50+'],
        descr: 'Calça ótima para trabalho e dia-a-dia.',
        localizacao: 'Shopping Estação - Loja: 06; Kira`s Clothing',
        telefone: '(41) 3847-9372',
        nome_img: 'Calca_Feminina_Social_Preta_e_Branca.png',
        sexo: 'feminino'
      },
      {
        tipo: "Camisa",
        titulo: 'Camisa LeLup Vermelha',
        valor_antes: 'R$160,00',
        valor_depois: 'R$80,00',
        off: '50%',
        cor: 'Vermelho',
        tamanhos: ['1', '2', '3', '4', '5', '6', '7'],
        descr: 'Camisa com caimento perfeito para o trabalho e dia-a-dia.',
        localizacao: 'Shopping Estação - Loja: 06; Kira`s Clothing',
        telefone: '(41) 3847-9372',
        nome_img: 'Camisa_Masculina_Vermelha.png',
        sexo: 'masculino'
      },
      {
        tipo: "Camisa",
          titulo: 'Camisa Woodpecker Verde',
        valor_antes: 'R$150,00',
        valor_depois: 'R$75,00',
        off: '50%',
        cor: 'Verde',
        tamanhos: ['1', '2', '3', '4', '5', '6', '7'],
        descr: 'Camisa com caimento perfeito para o trabalho e dia-a-dia.',
        localizacao: 'Shopping Estação - Loja: 06; Kira`s Clothing',
        telefone: '(41) 3847-9372',
        nome_img: 'Camisa_Masculina_Verde.png',
        sexo: 'masculino'
      },
      {
        tipo: "Camisa",
        titulo: 'Camisa Winn ed.4',
        valor_antes: 'R$199,90',
        valor_depois: 'R$90,00',
        off: '55%',
        cor: 'Estampada',
        tamanhos: ['1', '2', '3', '4', '5', '6', '7'],
        descr: 'Camisa com caimento perfeito para o trabalho e dia-a-dia.',
        localizacao: 'Shopping Estação - Loja: 06; Kira`s Clothing',
        telefone: '(41) 3847-9372',
        nome_img: 'Camisa_Masculina_Pontilhada.png',
        sexo: 'masculino'
      }, 
      {
        tipo: "Camisa",
        titulo: 'Camisa Azul',
        valor_antes: 'R$219,90',
        valor_depois: 'R$99,90',
        off: '55% ',
        cor: 'Azul',
        tamanhos: ['30-34', '36-38', '40-44', '46-50', '50+'],
        descr: 'Calça ótima para o dia-a-dia e conforto.',
        localizacao: 'Shopping Estação - Loja: 06; Kira`s Clothing',
        telefone: '(41) 3847-9372',
        nome_img: 'Camisa_Masculina_Azul.png',
        sexo: 'masculino'
      },
      {
        tipo: "Camisa",
        titulo: 'Camisa Azul Listrada',
        valor_antes: 'R$119,90',
        valor_depois: 'R$49,90',
        off: '58% ',
        cor: 'Azul',
        tamanhos: ['30-34', '36-38', '40-44', '46-50', '50+'],
        descr: 'Calça ótima para o dia-a-dia e conforto.',
        localizacao: 'Shopping Estação - Loja: 06; Kira`s Clothing',
        telefone: '(41) 3847-9372',
        nome_img: 'Camisa_Masculina_Azul_Listrada.png',
        sexo: 'masculino'
      },
      {
        tipo: "Camisa",
        titulo: 'Camisa Clara',
        valor_antes: 'R$259,90',
        valor_depois: 'R$119,90',
        off: '54% ',
        cor: 'Azul',
        tamanhos: ['30-34', '36-38', '40-44', '46-50', '50+'],
        descr: 'Calça ótima para o dia-a-dia e conforto.',
        localizacao: 'Shopping Estação - Loja: 06; Kira`s Clothing',
        telefone: '(41) 3847-9372',
        nome_img: 'Camisa_Masculina_Azul_Manga_Longa.png',
        sexo: 'masculino'
      },
      {
        tipo: "Camisa",
        titulo: 'Camisa com detalhe bege',
        valor_antes: 'R$199,90',
        valor_depois: 'R$89,90',
        off: '55% ',
        cor: 'Branca',
        tamanhos: ['30-34', '36-38', '40-44', '46-50', '50+'],
        descr: 'Calça ótima para o dia-a-dia e conforto.',
        localizacao: 'Shopping Estação - Loja: 06; Kira`s Clothing',
        telefone: '(41) 3847-9372',
        nome_img: 'Camisa_Masculina_Branca_Bege.png',
        sexo: 'masculino'
      },
      {
        tipo: "Camisa",
        titulo: 'Camisa Brilhante',
        valor_antes: 'R$159,90',
        valor_depois: 'R$69,90',
        off: '56% ',
        cor: 'Prata',
        tamanhos: ['30-34', '36-38', '40-44', '46-50', '50+'],
        descr: 'Calça ótima para o dia-a-dia e conforto.',
        localizacao: 'Shopping Estação - Loja: 06; Kira`s Clothing',
        telefone: '(41) 3847-9372',
        nome_img: 'Camisa_Masculina_Brilhante.png',
        sexo: 'masculino'
      },
      {
        tipo: "Camisa",
        titulo: 'Camisa Listrada Clara',
        valor_antes: 'R$329,90',
        valor_depois: 'R$129,90',
        off: '61%',
        cor: 'Azul',
        tamanhos: ['30-34', '36-38', '40-44', '46-50', '50+'],
        descr: 'Calça ótima para o dia-a-dia e conforto.',
        localizacao: 'Shopping Estação - Loja: 06; Kira`s Clothing',
        telefone: '(41) 3847-9372',
        nome_img: 'Camisa_Masculina_Listrada_Azul_Clara.png',
        sexo: 'masculino'
      },
      {
        tipo: "Camisa",
        titulo: 'Camisa Bees',
        valor_antes: 'R$150,00',
        valor_depois: 'R$50,00',
        off: '67%',
        cor: 'Rosa Estampada',
        tamanhos: ['1', '2', '3', '4', '5', '6', '7'],
        descr: 'Camisa com caimento perfeito para o trabalho e dia-a-dia.',
        localizacao: 'Shopping Estação - Loja: 06; Kira`s Clothing',
        telefone: '(41) 3847-9372',
        nome_img: 'Camisa_Feminina_Abelha.png',
        sexo: 'feminino'
      },
      {
        tipo: "Camisa",
        titulo: 'Camisa Renda',
        valor_antes: 'R$140,00',
        valor_depois: 'R$45,00',
        off: '68%',
        cor: 'Verde',
        tamanhos: ['1', '2', '3', '4', '5', '6', '7'],
        descr: 'Camisa com caimento perfeito para o trabalho e dia-a-dia.',
        localizacao: 'Shopping Estação - Loja: 06; Kira`s Clothing',
        telefone: '(41) 3847-9372',
        nome_img: 'Camisa_Feminina_Verde.png',
        sexo: 'feminino'
      },
      {
        tipo: "Camisa",
        titulo: 'Camisa Estampada Blanche`s',
        valor_antes: 'R$280,00',
        valor_depois: 'R$120,00',
        off: '58%',
        cor: 'Laranja Estampado',
        tamanhos: ['1', '2', '3', '4', '5', '6', '7'],
        descr: 'Camisa com caimento perfeito para o trabalho e dia-a-dia.',
        localizacao: 'Shopping Estação - Loja: 06; Kira`s Clothing',
        telefone: '(41) 3847-9372',
        nome_img: 'Camisa_Feminina_Onca.png',
        sexo: 'feminino'
      },
      {
        tipo: "Camisa",
        titulo: 'Camisa Xadrez',
        valor_antes: 'R$289,90',
        valor_depois: 'R$129,90',
        off: '55%',
        cor: 'Azul',
        tamanhos: ['1', '2', '3', '4', '5', '6', '7'],
        descr: 'Camisa com caimento perfeito para o trabalho e dia-a-dia.',
        localizacao: 'Shopping Estação - Loja: 06; Kira`s Clothing',
        telefone: '(41) 3847-9372',
        nome_img: 'Camisa_Feminina_Azul.png',
        sexo: 'feminino'
      },
      {
        tipo: "Camisa",
        titulo: 'Camisa Lisa',
        valor_antes: 'R$259,90',
        valor_depois: 'R$119,90',
        off: '54%',
        cor: 'Branca',
        tamanhos: ['1', '2', '3', '4', '5', '6', '7'],
        descr: 'Camisa com caimento perfeito para o trabalho e dia-a-dia.',
        localizacao: 'Shopping Estação - Loja: 06; Kira`s Clothing',
        telefone: '(41) 3847-9372',
        nome_img: 'Camisa_Feminina_Branca.png',
        sexo: 'feminino'
      },
      {
        tipo: "Camisa",
        titulo: 'Camisa Floral',
        valor_antes: 'R$139,90',
        valor_depois: 'R$49,90',
        off: '64%',
        cor: 'Rosa',
        tamanhos: ['1', '2', '3', '4', '5', '6', '7'],
        descr: 'Camisa com caimento perfeito para o trabalho e dia-a-dia.',
        localizacao: 'Shopping Estação - Loja: 06; Kira`s Clothing',
        telefone: '(41) 3847-9372',
        nome_img: 'Camisa_Feminina_Floral.png',
        sexo: 'feminino'
      }, 
      {
        tipo: "Camisa",
        titulo: 'Camisa Listrada',
        valor_antes: 'R$299,90',
        valor_depois: 'R$89,90',
        off: '70%',
        cor: 'Cinza',
        tamanhos: ['1', '2', '3', '4', '5', '6', '7'],
        descr: 'Camisa com caimento perfeito para o trabalho e dia-a-dia.',
        localizacao: 'Shopping Estação - Loja: 06; Kira`s Clothing',
        telefone: '(41) 3847-9372',
        nome_img: 'Camisa_Feminina_Listrada_Cinza.png',
        sexo: 'feminino'
      },
      {
        tipo: "Camisa",
        titulo: 'Camisa',
        valor_antes: 'R$259,90',
        valor_depois: 'R$79,90',
        off: '69%',
        cor: 'Preta',
        tamanhos: ['1', '2', '3', '4', '5', '6', '7'],
        descr: 'Camisa com caimento perfeito para o trabalho e dia-a-dia.',
        localizacao: 'Shopping Estação - Loja: 06; Kira`s Clothing',
        telefone: '(41) 3847-9372',
        nome_img: 'Camisa_Feminina_Preta.png',
        sexo: 'feminino'
      },
      {
        tipo: "Camisa",
        titulo: 'Camisa Xadrez',
        valor_antes: 'R$149,90',
        valor_depois: 'R$39,90',
        off: '73%',
        cor: 'Branca e Azul',
        tamanhos: ['1', '2', '3', '4', '5', '6', '7'],
        descr: 'Camisa com caimento perfeito para o trabalho e dia-a-dia.',
        localizacao: 'Shopping Estação - Loja: 06; Kira`s Clothing',
        telefone: '(41) 3847-9372',
        nome_img: 'Camisa_Feminina_Xadrez_Azul.png',
        sexo: 'feminino'
      },
      {
        tipo: 'Camiseta',
        titulo: 'Camiseta Fortnite 1st Edition',
        valor_antes: 'R$90,00',
        valor_depois: 'R$40,00',
        off: '56%',
        cor: 'Roxo Estampado',
        tamanhos: ['pp', 'p', 'm', 'g', 'gg'],
        descr: 'Combina com qualquer outra roupa, versátil e completa.',
        localizacao: 'Shopping Estação - Loja: 06; Kira`s Clothing',
        telefone: '(41) 3847-9372',
        nome_img: 'Camiseta_Masculina_Fortnite.png',
        sexo: 'masculino'
      },
      {
        tipo: 'Camiseta',
        titulo: 'Camiseta Paladins 1st Edition',
        valor_antes: 'R$99,90',
        valor_depois: 'R$39,90',
        off: '60%',
        cor: 'Azul',
        tamanhos: ['pp', 'p', 'm', 'g', 'gg'],
        descr: 'Combina com qualquer outra roupa, versátil e completa.',
        localizacao: 'Shopping Estação - Loja: 06; Kira’s Clothing',
        telefone: '(41) 3847-9372',
        nome_img: 'Camiseta_Masculina_Paladins.png',
        sexo: 'masculino'
      },
      {
        tipo: 'Camiseta',
        titulo: 'Camiseta Overwatch 1st Edition',
        valor_antes: 'R$90,00',
        valor_depois: 'R$40,00',
        off: '56%',
        cor: 'Preto Estampado',
        tamanhos: ['pp', 'p', 'm', 'g', 'gg'],
        descr: 'Combina com qualquer outra roupa, versátil e completa.',
        localizacao: 'Shopping Estação - Loja: 06; Kira`s Clothing',
        telefone: '(41) 3847-9372',
        nome_img: 'Camiseta_Masculina_Overwatch.png',
        sexo: 'masculino'
      },
      {
        tipo: 'Camiseta',
        titulo: 'Camiseta Camuflada Skull',
        valor_antes: 'R$180,00',
        valor_depois: 'R$85,00',
        off: '53%',
        cor: 'Preto Estampado',
        tamanhos: ['pp', 'p', 'm', 'g', 'gg'],
        descr: 'Combina com qualquer outra roupa, versátil e completa.',
        localizacao: 'Shopping Estação - Loja: 06; Kira`s Clothing',
        telefone: '(41) 3847-9372',
        nome_img: 'Camiseta_Masculina_Exercito.png',
        sexo: 'masculino'
      },
      {
        tipo: 'Camiseta',
        titulo: 'Camiseta Chewbacca',
        valor_antes: 'R$99,90',
        valor_depois: 'R$39,90',
        off: '60%',
        cor: 'Branca',
        tamanhos: ['pp', 'p', 'm', 'g', 'gg'],
        descr: 'Combina com qualquer outra roupa, versátil e completa.',
        localizacao: 'Shopping Estação - Loja: 06; Kira`s Clothing',
        telefone: '(41) 3847-9372',
        nome_img: 'Camiseta_Masculina_Branca_Chewbacca.png',
        sexo: 'masculino'
      },
      {
        tipo: 'Camiseta',
        titulo: 'Camiseta Elefante',
        valor_antes: 'R$99,90',
        valor_depois: 'R$39,90',
        off: '60%',
        cor: 'Branca',
        tamanhos: ['pp', 'p', 'm', 'g', 'gg'],
        descr: 'Combina com qualquer outra roupa, versátil e completa.',
        localizacao: 'Shopping Estação - Loja: 06; Kira`s Clothing',
        telefone: '(41) 3847-9372',
        nome_img: 'Camiseta_Masculina_Branca_Elefante.png',
        sexo: 'masculino'
      },
      {
        tipo: 'Camiseta',
        titulo: 'Camiseta Lobo',
        valor_antes: 'R$119,90',
        valor_depois: 'R$49,90',
        off: '60%',
        cor: 'Branca',
        tamanhos: ['pp', 'p', 'm', 'g', 'gg'],
        descr: 'Combina com qualquer outra roupa, versátil e completa.',
        localizacao: 'Shopping Estação - Loja: 06; Kira`s Clothing',
        telefone: '(41) 3847-9372',
        nome_img: 'Camiseta_Masculina_Branca_Lobo.png',
        sexo: 'masculino'
      },
      {
        tipo: 'Camiseta',
        titulo: 'Camiseta Lobo',
        valor_antes: 'R$119,90',
        valor_depois: 'R$49,90',
        off: '58%',
        cor: 'Branca',
        tamanhos: ['pp', 'p', 'm', 'g', 'gg'],
        descr: 'Combina com qualquer outra roupa, versátil e completa.',
        localizacao: 'Shopping Estação - Loja: 06; Kira`s Clothing',
        telefone: '(41) 3847-9372',
        nome_img: 'Camiseta_Masculina_Branca_Urso.png',
        sexo: 'masculino'
      },
      {
        tipo: 'Camiseta',
        titulo: 'Camiseta Cachorro',
        valor_antes: 'R$159,90',
        valor_depois: 'R$49,90',
        off: '69%',
        cor: 'Branca',
        tamanhos: ['pp', 'p', 'm', 'g', 'gg'],
        descr: 'Combina com qualquer outra roupa, versátil e completa.',
        localizacao: 'Shopping Estação - Loja: 06; Kira`s Clothing',
        telefone: '(41) 3847-9372',
        nome_img: 'Camiseta_Masculina_Cachorro_Branca.png',
        sexo: 'masculino'
      },
      {
        tipo: 'Camiseta',
        titulo: 'Camiseta Elefante',
        valor_antes: 'R$159,90',
        valor_depois: 'R$59,90',
        off: '63%',
        cor: 'Branca',
        tamanhos: ['pp', 'p', 'm', 'g', 'gg'],
        descr: 'Combina com qualquer outra roupa, versátil e completa.',
        localizacao: 'Shopping Estação - Loja: 06; Kira`s Clothing',
        telefone: '(41) 3847-9372',
        nome_img: 'Camiseta_Masculina_Preta_Elefante.png',
        sexo: 'masculino'
      },
      {
        tipo: 'Camiseta',
        titulo: 'Camiseta Nike fit-4',
        valor_antes: 'R$189,90',
        valor_depois: 'R$65,50',
        off: '66%',
        cor: 'Rosa',
        tamanhos: ['pp', 'p', 'm', 'g', 'gg'],
        descr: 'Combina com qualquer outra roupa, versátil e completa.',
        localizacao: 'Shopping Estação - Loja: 06; Kira`s Clothing',
        telefone: '(41) 3847-9372',
        nome_img: 'Camiseta_Feminina_Nike.png',
        sexo: 'feminino'
      },
      {
        tipo: 'Camiseta',
        titulo: 'Camiseta Brasil Verde',
        valor_antes: 'R$120,00',
        valor_depois: 'R$40,00',
        off: '66%',
        cor: 'Verde Estampado',
        tamanhos: ['pp', 'p', 'm', 'g', 'gg'],
        descr: 'Combina com qualquer outra roupa, versátil e completa.',
        localizacao: 'Shopping Estação - Loja: 06; Kira`s Clothing',
        telefone: '(41) 3847-9372',
        nome_img: 'Camiseta_Feminina_Verde.png',
        sexo: 'feminino'
      },
      {
        tipo: 'Camiseta',
        titulo: 'Camiseta Roses',
        valor_antes: 'R$160,00',
        valor_depois: 'R$69,90',
        off: '43%',
        cor: 'Branco Bordado',
        tamanhos: ['pp', 'p', 'm', 'g', 'gg'],
        descr: 'Combina com qualquer outra roupa, versátil e completa.',
        localizacao: 'Shopping Estação - Loja: 06; Kira`s Clothing',
        telefone: '(41) 3847-9372',
        nome_img: 'Camiseta_Feminina_Flor.png',
        sexo: 'feminino'
      },
      {
        tipo: 'Camiseta',
        titulo: 'Camiseta Bolinhas',
        valor_antes: 'R$169,90',
        valor_depois: 'R$69,90',
        off: '59%',
        cor: 'Branca',
        tamanhos: ['pp', 'p', 'm', 'g', 'gg'],
        descr: 'Combina com qualquer outra roupa, versátil e completa.',
        localizacao: 'Shopping Estação - Loja: 06; Kira`s Clothing',
        telefone: '(41) 3847-9372',
        nome_img: 'Camiseta_Feminina_Branca_Bolinhas.png',
        sexo: 'feminino'
      },
      {
        tipo: 'Camiseta',
        titulo: 'Camiseta Paws',
        valor_antes: 'R$99,90',
        valor_depois: 'R$29,90',
        off: '70%',
        cor: 'Branca',
        tamanhos: ['pp', 'p', 'm', 'g', 'gg'],
        descr: 'Combina com qualquer outra roupa, versátil e completa.',
        localizacao: 'Shopping Estação - Loja: 06; Kira`s Clothing',
        telefone: '(41) 3847-9372',
        nome_img: 'Camiseta_Feminina_Branca_Paws.png',
        sexo: 'feminino'
      },
      {
        tipo: 'Camiseta',
        titulo: 'Camiseta Snoopy',
        valor_antes: 'R$79,90',
        valor_depois: 'R$19,90',
        off: '75%',
        cor: 'Branca',
        tamanhos: ['pp', 'p', 'm', 'g', 'gg'],
        descr: 'Combina com qualquer outra roupa, versátil e completa.',
        localizacao: 'Shopping Estação - Loja: 06; Kira`s Clothing',
        telefone: '(41) 3847-9372',
        nome_img: 'Camiseta_Feminina_Branca_Snoopy.png',
        sexo: 'feminino'
      },
      {
        tipo: 'Camiseta',
        titulo: 'Camiseta Ultima Limpa',
        valor_antes: 'R$89,90',
        valor_depois: 'R$29,90',
        off: '67%',
        cor: 'Branca',
        tamanhos: ['pp', 'p', 'm', 'g', 'gg'],
        descr: 'Combina com qualquer outra roupa, versátil e completa.',
        localizacao: 'Shopping Estação - Loja: 06; Kira`s Clothing',
        telefone: '(41) 3847-9372',
        nome_img: 'Camiseta_Feminina_Branca_Ultimalimpa.png',
        sexo: 'feminino'
      },
      {
        tipo: 'Camiseta',
        titulo: 'Camiseta Ancora',
        valor_antes: 'R$89,90',
        valor_depois: 'R$29,90',
        off: '67%',
        cor: 'Preta',
        tamanhos: ['pp', 'p', 'm', 'g', 'gg'],
        descr: 'Combina com qualquer outra roupa, versátil e completa.',
        localizacao: 'Shopping Estação - Loja: 06; Kira`s Clothing',
        telefone: '(41) 3847-9372',
        nome_img: 'Camiseta_Feminina_Preta_Ancora.png',
        sexo: 'feminino'
      },
      {
        tipo: 'Camiseta',
        titulo: 'Camiseta Cat Flag',
        valor_antes: 'R$99,90',
        valor_depois: 'R$39,90',
        off: '60%',
        cor: 'Preta',
        tamanhos: ['pp', 'p', 'm', 'g', 'gg'],
        descr: 'Combina com qualquer outra roupa, versátil e completa.',
        localizacao: 'Shopping Estação - Loja: 06; Kira`s Clothing',
        telefone: '(41) 3847-9372',
        nome_img: 'Camiseta_Feminina_Preta_Cat_Flag.png',
        sexo: 'feminino'
      },
      {
        tipo: 'Casaco',
        titulo: 'Casaco Warpeace',
        valor_antes: 'R$230,00',
        valor_depois: 'R$150,00',
        off: '35%',
        cor: 'Verde',
        tamanhos: ['pp', 'p', 'm', 'g', 'gg'],
        descr: 'Casaco perfeito para enfrentar o frio de Curitiba sem perder o estilo.',
        localizacao: 'Shopping Estação - Loja: 06; Kira`s Clothing',
        telefone: '(41) 3847-9372',
        nome_img: 'Casaco_Masculino_Verde.png',
        sexo: 'masculino'
      },
      {
        tipo: 'Casaco',
        titulo: 'Jaqueta North Face ed.39',
        valor_antes: 'R$359,90',
        valor_depois: 'R$180,00',
        off: '50%',
        cor: 'Preto',
        tamanhos: ['pp', 'p', 'm', 'g', 'gg'],
        descr: 'Jaqueta perfeito para enfrentar o frio de Curitiba sem perder o estilo.',
        localizacao: 'Shopping Estação - Loja: 06; Kira`s Clothing',
        telefone: '(41) 3847-9372',
        nome_img: 'Casaco_Masculino_North.png',
        sexo: 'masculino'
      },
      {
        tipo: 'Casaco',
        titulo: 'Blazer de veludo John`s',
        valor_antes: 'R$499,90',
        valor_depois: 'R$280,00',
        off: '44%',
        cor: 'Vermelho',
        tamanhos: ['pp', 'p', 'm', 'g', 'gg'],
        descr: 'Blazer perfeito para enfrentar o frio de Curitiba sem perder o estilo.',
        localizacao: 'Shopping Estação - Loja: 06; Kira`s Clothing',
        telefone: '(41) 3847-9372',
        nome_img: 'Casaco_Masculino_Tux.png',
        sexo: 'masculino'
      },
      {
        tipo: 'Casaco',
        titulo: 'Jaqueta com capuz',
        valor_antes: 'R$399,90',
        valor_depois: 'R$189,90',
        off: '52%',
        cor: 'Amarela',
        tamanhos: ['pp', 'p', 'm', 'g', 'gg'],
        descr: 'Jaqueta perfeita para enfrentar o frio de Curitiba sem perder o estilo.',
        localizacao: 'Shopping Estação - Loja: 06; Kira`s Clothing',
        telefone: '(41) 3847-9372',
        nome_img: 'Casaco_Masculino_Amarelo.png',
        sexo: 'masculino'
      },
      {
        tipo: 'Casaco',
        titulo: 'Jaqueta com capuz',
        valor_antes: 'R$399,90',
        valor_depois: 'R$189,90',
        off: '52%',
        cor: 'Azul',
        tamanhos: ['pp', 'p', 'm', 'g', 'gg'],
        descr: 'Jaqueta perfeita para enfrentar o frio de Curitiba sem perder o estilo.',
        localizacao: 'Shopping Estação - Loja: 06; Kira`s Clothing',
        telefone: '(41) 3847-9372',
        nome_img: 'Casaco_Masculino_Azul.png',
        sexo: 'masculino'
      },
      {
        tipo: 'Casaco',
        titulo: 'Jaqueta College',
        valor_antes: 'R$369,90',
        valor_depois: 'R$169,90',
        off: '54%',
        cor: 'Bordô',
        tamanhos: ['pp', 'p', 'm', 'g', 'gg'],
        descr: 'Jaqueta perfeita para enfrentar o frio de Curitiba sem perder o estilo.',
        localizacao: 'Shopping Estação - Loja: 06; Kira`s Clothing',
        telefone: '(41) 3847-9372',
        nome_img: 'Casaco_Masculino_Bordo_College.png',
        sexo: 'masculino'
      },
      {
        tipo: 'Casaco',
        titulo: 'Jaqueta College',
        valor_antes: 'R$369,90',
        valor_depois: 'R$169,90',
        off: '54%',
        cor: 'Cinza',
        tamanhos: ['pp', 'p', 'm', 'g', 'gg'],
        descr: 'Jaqueta perfeita para enfrentar o frio de Curitiba sem perder o estilo.',
        localizacao: 'Shopping Estação - Loja: 06; Kira`s Clothing',
        telefone: '(41) 3847-9372',
        nome_img: 'Casaco_Masculino_Cinza_College.png',
        sexo: 'masculino'
      },
      {
        tipo: 'Casaco',
        titulo: 'Jaqueta com capuz',
        valor_antes: 'R$299,90',
        valor_depois: 'R$129,90',
        off: '57%',
        cor: 'Preta',
        tamanhos: ['pp', 'p', 'm', 'g', 'gg'],
        descr: 'Jaqueta perfeita para enfrentar o frio de Curitiba sem perder o estilo.',
        localizacao: 'Shopping Estação - Loja: 06; Kira`s Clothing',
        telefone: '(41) 3847-9372',
        nome_img: 'Casaco_Masculino_Preto.png',
        sexo: 'masculino'
      },
      {
        tipo: 'Casaco',
        titulo: 'Jaqueta Lobo',
        valor_antes: 'R$599,90',
        valor_depois: 'R$299,90',
        off: '50%',
        cor: 'Preta',
        tamanhos: ['pp', 'p', 'm', 'g', 'gg'],
        descr: 'Jaqueta perfeita para enfrentar o frio de Curitiba sem perder o estilo.',
        localizacao: 'Shopping Estação - Loja: 06; Kira`s Clothing',
        telefone: '(41) 3847-9372',
        nome_img: 'Casaco_Masculino_Preto_Lobo.png',
        sexo: 'masculino'
      },
      {
        tipo: 'Casaco',
        titulo: 'Casaco Cinza Keller`s',
        valor_antes: 'R$430,00',
        valor_depois: 'R$180,00',
        off: '59%',
        cor: 'Cinza',
        tamanhos: ['pp', 'p', 'm', 'g', 'gg'],
        descr: 'Casaco perfeito para enfrentar o frio de Curitiba sem perder o estilo.',
        localizacao: 'Shopping Estação - Loja: 06; Kira`s Clothing',
        telefone: '(41) 3847-9372',
        nome_img: 'Casaco_Feminino_Cinza.png',
        sexo: 'feminino'
      },
      {
        tipo: 'Casaco',
        titulo: 'Jaqueta CatCold 559',
        valor_antes: 'R$220,00',
        valor_depois: 'R$100,00',
        off: '55%',
        cor: 'Preto',
        tamanhos: ['pp', 'p', 'm', 'g', 'gg'],
        descr: 'Jaqueta perfeita para enfrentar o frio de Curitiba sem perder o estilo.',
        localizacao: 'Shopping Estação - Loja: 06; Kira`s Clothing',
        telefone: '(41) 3847-9372',
        nome_img: 'Casaco_Feminino_Cat.png',
        sexo: 'feminino'
      },
      {
        tipo: 'Casaco',
        titulo: 'Blazer Patrice`s 339',
        valor_antes: 'R$340,00',
        valor_depois: 'R$180,00',
        off: '47%',
        cor: 'Bege',
        tamanhos: ['pp', 'p', 'm', 'g', 'gg'],
        descr: 'Casaco perfeito para enfrentar o frio de Curitiba sem perder o estilo.',
        localizacao: 'Shopping Estação - Loja: 06; Kira`s Clothing',
        telefone: '(41) 3847-9372',
        nome_img: 'Casaco_Feminino_Bege.png',
        sexo: 'feminino'
      },
      {
        tipo: 'Casaco',
        titulo: 'Casaco com capuz',
        valor_antes: 'R$349,90',
        valor_depois: 'R$179,90',
        off: '49%',
        cor: 'Branca',
        tamanhos: ['pp', 'p', 'm', 'g', 'gg'],
        descr: 'Casaco perfeito para enfrentar o frio de Curitiba sem perder o estilo.',
        localizacao: 'Shopping Estação - Loja: 06; Kira`s Clothing',
        telefone: '(41) 3847-9372',
        nome_img: 'Casaco_Feminino_Chuva.png',
        sexo: 'feminino'
      },
      {
        tipo: 'Casaco',
        titulo: 'Casaco',
        valor_antes: 'R$199,90',
        valor_depois: 'R$59,90',
        off: '70%',
        cor: 'Cinza',
        tamanhos: ['pp', 'p', 'm', 'g', 'gg'],
        descr: 'Casaco perfeito para enfrentar o frio de Curitiba sem perder o estilo.',
        localizacao: 'Shopping Estação - Loja: 06; Kira`s Clothing',
        telefone: '(41) 3847-9372',
        nome_img: 'Casaco_Feminino_Cinza.png',
        sexo: 'feminino'
      },
      {
        tipo: 'Casaco',
        titulo: 'Casaco Felpudo',
        valor_antes: 'R$199,90',
        valor_depois: 'R$59,90',
        off: '70%',
        cor: 'Preto',
        tamanhos: ['pp', 'p', 'm', 'g', 'gg'],
        descr: 'Casaco perfeito para enfrentar o frio de Curitiba sem perder o estilo.',
        localizacao: 'Shopping Estação - Loja: 06; Kira`s Clothing',
        telefone: '(41) 3847-9372',
        nome_img: 'Casaco_Feminino_Preto.png',
        sexo: 'feminino'
      },
      {
        tipo: 'Casaco',
        titulo: 'Casaco Inverno',
        valor_antes: 'R$329,90',
        valor_depois: 'R$159,90',
        off: '52%',
        cor: 'Preto',
        tamanhos: ['pp', 'p', 'm', 'g', 'gg'],
        descr: 'Casaco perfeito para enfrentar o frio de Curitiba sem perder o estilo.',
        localizacao: 'Shopping Estação - Loja: 06; Kira`s Clothing',
        telefone: '(41) 3847-9372',
        nome_img: 'Casaco_Feminino_Preto.png',
        sexo: 'feminino'
      },
      {
        tipo: 'Casaco',
        titulo: 'Casaco Claro',
        valor_antes: 'R$499,90',
        valor_depois: 'R$159,90',
        off: '68%',
        cor: 'Rosa',
        tamanhos: ['pp', 'p', 'm', 'g', 'gg'],
        descr: 'Casaco perfeito para enfrentar o frio de Curitiba sem perder o estilo.',
        localizacao: 'Shopping Estação - Loja: 06; Kira`s Clothing',
        telefone: '(41) 3847-9372',
        nome_img: 'Casaco_Feminino_Rosa_Claro.png',
        sexo: 'feminino'
      },
      {
        tipo: 'Casaco',
        titulo: 'Casaco Elegane',
        valor_antes: 'R$399,90',
        valor_depois: 'R$149,90',
        off: '50%',
        cor: 'Vermelho',
        tamanhos: ['pp', 'p', 'm', 'g', 'gg'],
        descr: 'Casaco perfeito para enfrentar o frio de Curitiba sem perder o estilo.',
        localizacao: 'Shopping Estação - Loja: 06; Kira`s Clothing',
        telefone: '(41) 3847-9372',
        nome_img: 'Casaco_Feminino_Vermelho.png',
        sexo: 'feminino'
      }
    ],
    filteredItems: [],
    chosenItem: {}, //Non-reactive ainda.
    filterTags: [],
    galleryPageItemCount:1, 
  },
  mutations: {
    addItem (state, item) {
      let found = state.items.find((el) => {
        return el.titulo == item.titulo
      })
      if (found) return

      state.items.push(item)

    },
    removeItem (state, item) {
      state.items.find((el, index, arr) => {
        if (el && el.titulo && el.titulo === item.titulo) {
          arr.splice(index, 1)
        }
      })
    },
    addItemsToFilter (state, item_arr) {
      state.filteredItems = item_arr
    },
    removeItemsOverlays (state) {
      state.items.forEach(item => {
        item.overlayVisible = false
      })
    },
    refreshFilteredItemOnCartChange (state, item) {
      state.filteredItems.find((el, index, arr) => {
        if (el && el.titulo && el.titulo === item.titulo) {
          item.oncart = false
          arr.splice(index, 1)
          setTimeout(() => {
            arr.splice(index, 0, item)
          }, 100)
        }
      })
    },
    chosenItem (state, item) {
      state.chosenItem = item
    },

    //Filtro de itens
    addFilterTag (state, tag) {
      if(state.filterTags.indexOf(tag) === -1) state.filterTags.push(tag)
    },
    removeFilterTag (state, tag) {
      let index = state.filterTags.indexOf(tag)
      if ( index !== -1) state.filterTags.splice(index, 1)
    },
    removeAllFilterTags (state) {
      state.filterTags = []
    },
    applyFilterTags (state) {
      state.filteredItems = []

      // Se nao tem tags, mostra tudo
      if (state.filterTags.length === 0) {
        state.filteredItems = state.items
        return
      }

      state.items.forEach(item => {
        let pushItem = false;

        state.filterTags.forEach (tag => {
          let lower_tag = tag.toLowerCase()
          let tag_arr = lower_tag.split(' ')
          
          if (tag_arr.length === 2) {
            var tipo = tag_arr[0]
            var sexo = tag_arr[1]

            // Busca sexo (somente o sexo escolhido)
            if (item.sexo.toLowerCase().indexOf(sexo) === -1) {
              return
            }
            if (item.tipo.toLowerCase().indexOf(tipo) === -1) {
              return
            }
            pushItem = true
            return
          }
        })
        if( pushItem) { 
          state.filteredItems.push(item)
        }
      })
    },
    changeGalleryItemCountPage(state, value) {
      state.galleryPageItemCount = value
    }
  },
  getters: {
    getItems (state) {
      return state.items
    },
    getItemsCount (state) {
      return state.items.length
    },
    getFilteredItems (state) {
      return state.filteredItems
    },
    getFilteredItemsCount (state) {
      return state.filteredItems.length
    },
    getChosenItem (state) {
      return state.chosenItem
    },
    getFilterTags (state) {
      return state.filterTags
    },
    getGalleryPageItemCount (state) {
      return state.galleryPageItemCount
    }
  },
  actions: {
    async fetchItemFromServer({state, commit}, item_obj) {
      // item_obj = {
      //   id: String,
      //   seller_code: String
      // }
      
      //TODO: Buscar o item no server com id e seller_code.
      // await ...
      let item = state.items.find((state_item) => {
        if (state_item.titulo === item_obj.id) {
          return state_item
        }
      })
      commit ('chosenItem', item)
      return item
    },
    addAndCalculateFilterTag ({commit}, tag) {
      commit('addFilterTag', tag)
      commit('applyFilterTags')
    },
    removeAndCalculateFilterTag ({commit}, tag) {
      commit('removeFilterTag', tag)
      commit('applyFilterTags')
    },

  }
}
