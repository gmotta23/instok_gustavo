export default {
  state: {
    cart: []
  },
  mutations: {
    addToCart (state, item) {
      let found = state.cart.find((el) => {
        return el.titulo == item.titulo
      })
      if (found) return
      item.oncart = true
      state.cart.push(item)
      
    },
    removeFromCart (state, item) {
      state.cart.find((el, index, arr) => {
        if (el && el.titulo && el.titulo === item.titulo) {
          item.oncart = false
          arr.splice(index, 1)
        }
      })
    },
    clearCart (state) {
      state.cart = []
    }
  },
  getters: {
    getCartItems (state) {
      return state.cart
    },
    getCartItemsCount (state) {
      return state.cart.length
    },
    getCartItemValueSums (state) {
      let sum_antes = 0
      let sum_depois = 0
      let sum_diff = 0

      state.cart.forEach(cartItem => {
        sum_antes += parseFloat(cartItem.valor_antes.replace('R$', '').replace(',', '.')) * 100
        sum_depois += parseFloat(cartItem.valor_depois.replace('R$', '').replace(',', '.')) * 100
      })

      sum_diff = sum_antes-sum_depois
      return {
        sum_antes: sum_antes / 100,
        sum_depois: sum_depois / 100,
        sum_diff: sum_diff / 100
      }
    }
  },
  actions: {
    fetchCartFromServer () {

    },
    sendCartToServer () {

    },
    removeFromCart ({state, commit}, item) {
      commit('removeFromCart',item)
      commit('refreshFilteredItemOnCartChange', item)
    }
  }
}
