export default {
  state: {
    email: '',
    name: '',
    access_token: '',
    type: ''
  },
  mutations: {
    registerUser (state, user_data) {
      state.email = user_data.email
      state.name = user_data.name
      state.access_token = ''
      state.type = user_data.type
    },
    unregisterUser (state) {
      state.email = ''
      state.name = ''
      state.access_token = ''
      state.type = ''
    },
  },
  getters: {
    getUser (state) {
      return state
    }
  },
  actions: {
    async login ({ commit }, user_data) {
      // Simulando acesso ao servidor
      await new Promise( (resolve) => {setTimeout(() => {
        user_data.type = 'consumidor'
        resolve()
      }, 1000)})

      commit('registerUser', user_data)
    },
    logout ({commit}) {
      commit('unregisterUser')
    }
  }
}
