import Vuex from 'vuex'
import createPersistedState from 'vuex-persistedstate'

import cartModule from '@/store/cart'
import itemsModule from '@/store/items'
import userModule from '@/store/user'

if ( process.browser ) {

}

const createStore = () => {
  return new Vuex.Store({
    state: {
      
    },
    mutations: {
     
    },
    modules: {
      cartModule,
      itemsModule,
      userModule
    }
  })
}


export default createStore
