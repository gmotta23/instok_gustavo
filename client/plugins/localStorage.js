import createPersistedState from 'vuex-persistedstate'

export default ({store}) => {
  createPersistedState({
      key: 'cart',
      reducer: state => ({
        cartModule: state.cartModule,
        // userModule: state.userModule, TODO: Pensar se vale a pena salvar o user
      })
  })(store)
}
